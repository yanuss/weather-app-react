import React, { Component } from "react";

import WeatherBox from "./WeatherBox";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import InputField from "./InputField";
import { apiUrl, apiKey, metric, citiesArr } from "../config";

const SearchBar = props => {
  const { cities, getAllCitites } = props;
  return (
    <div>
      <InputField />
      <Button variant="contained" onClick={getAllCitites}>
        Get weather
      </Button>
      <Button variant="contained">Sort by temp</Button>
      <Button variant="contained">Sort by Name</Button>
      <div>
        {cities.length > 0 && cities[0].name
          ? cities.map(weather => (
              <WeatherBox weather={weather} key={weather.id} />
            ))
          : ""}
      </div>
    </div>
  );
};

export default SearchBar;
