import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import Clear from "@material-ui/icons/Clear";

const styles = theme => ({
  card: {
    minWidth: 275,
    position: "relative"
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    marginBottom: 16,
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  delete: {
    position: "absolute",
    right: theme.spacing.unit * 2,
    top: theme.spacing.unit * 2
  }
});

const WeatherBox = props => {
  const { classes } = props;
  const bull = <span className={classes.bullet}>•</span>;
  console.log(props);
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title} color="textSecondary">
          Weather in {props.weather.name}
        </Typography>
        <Typography variant="headline" component="h2">
          Temp: {props.weather.main.temp} C
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Weather: {props.weather.weather["0"].description}
        </Typography>
        <Typography component="p">
          Wind {props.weather.wind.speed} m/s Coordinates:
        </Typography>
        <Typography component="p">
          Pressure: {props.weather.main.pressure}
        </Typography>
        <Typography component="p">
          Humidity: {props.weather.main.humidity}
        </Typography>
        Latitude: {props.weather.coord.lat}
        <Typography component="p">
          Longitude: {props.weather.coord.lon}{" "}
        </Typography>
        <Button variant="fab" aria-label="Delete" className={classes.delete}>
          <Clear />
        </Button>
      </CardContent>
    </Card>
  );
};

WeatherBox.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(WeatherBox);
