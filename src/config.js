export const apiUrl = "https:/api.openweathermap.org/data/2.5/group?id=";
export const apiUrlCity = "https:/api.openweathermap.org/data/2.5/weather?q=";
export const apiKey = "&APPID=8496070a6ed3bd27bd80c6c4595509b1";
const city = "Krakow";
export const citiesArr = [
  {
    id: 776069,
    name: "Białystok"
  },
  {
    id: 3102014,
    name: "Bydgoszcz"
  },
  {
    id: 3099434,
    name: "Gdansk"
  },
  {
    id: 3098722,
    name: "Gorzow Wielkopolski"
  },
  {
    id: 3096472,
    name: "Katowice"
  },
  {
    id: 769250,
    name: "Kielce"
  },
  {
    id: 3094802,
    name: "Krakow"
  },
  {
    id: 765876,
    name: "Lublin"
  },
  {
    id: 3093133,
    name: "Lodz"
  },
  {
    id: 763166,
    name: "Olsztyn"
  },
  {
    id: 3090048,
    name: "Opole"
  },
  {
    id: 3088171,
    name: "Poznan"
  },
  {
    id: 759734,
    name: "Rzeszow"
  },
  {
    id: 3083829,
    name: "Szczecin"
  },
  {
    id: 3083271,
    name: "Torun"
  },
  {
    id: 6695624,
    name: "Warszawa"
  },
  {
    id: 3081368,
    name: "Wroclaw"
  },
  {
    id: 7530991,
    name: "Zielona Góra"
  }
];
export const metric = "&units=metric";
export const htmlMode = "&mode=html";
//api.openweathermap.org/data/2.5/weather?q=London
//api.openweathermap.org/data/2.5/weather?lat=35&lon=139
//http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric

export function cityWeatherApi(city) {
  fetch(`${apiUrl}${city}${apiKey}${metric}`)
    .then(results => results.json())
    .then(data =>
      this.setState({
        weather: data
      })
    )
    .catch(console.log("err"));
}

// export const cities = {

// }

// axios.get(`${apiUrl}${city}${apiKey}${metric}`, {
//   mode: "cors"
// })
//   .then(response => {
//     if (!response.ok) {
//       throw Error("Network request failed");
//     }
//     return response;
//   })
//   .then(
//     data => {
//       this.setState({
//         weather: data
//       });
//     },
//     () => {
//       this.setState({
//         requestFailed: true
//       });
//     }
//   );
// //.catch(Error("error"));
// };
