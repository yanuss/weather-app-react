import React, { Component } from "react";

import InputField from "./components/InputField";
import WeatherBox from "./components/WeatherBox";
import Loading from "./components/Loading";
import axios from "axios";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";

import { apiUrl, apiUrlCity, apiKey, metric, citiesArr } from "./config";
import { compareAsc } from "date-fns";

const styles = theme => ({
  weatherList: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    maxWidth: "100%",
    flexWrap: "wrap"
  }
});
class App extends Component {
  state = {
    cities: [],
    data: [],
    city: "",
    requestFailed: false,
    citiesData: false
  };

  getData = (api, city) => {
    axios
      .get(`${api}${city}${apiKey}${metric}`, {
        mode: "cors"
      })
      .then(response => {
        if (response.statusText === "ok") {
          throw Error("Network request failed");
        }
        return response;
      })
      .then(
        response => {
          response.data.name
            ? this.setState({
                data: response.data
              })
            : this.setState({
                data: response.data.list
              });
        },
        () => {
          this.setState({
            requestFailed: true
          });
        }
      );
  };

  getAllCitites(citiesList) {
    const ids = citiesList.map(data => {
      return `${data.id}`;
    });
    this.getData(apiUrl, ids);
  }

  getCityName = event => {
    this.getData(apiUrlCity, this.state.city);
  };

  handleChange = event => {
    let cityName = event.target.value;
    this.setState({
      city: cityName
    });
  };

  sortByTemp() {
    const dataToSort = this.state.data;
    const ascending = (a, b, type) => {
      return a.main.temp - b.main.temp;
    };
    const sorted = dataToSort.map(el => el).sort(ascending);
    this.setState({
      data: sorted
    });
  }

  render() {
    const { requestFailed, data } = this.state;
    const { classes, theme } = this.props;
    if (requestFailed) return <p> Weather not found </p>;
    // if (citiesData) return <p> Loading cities failed</p>;
    // if (!city) return <Loading cities="city" />;
    // if (!data) return <Loading weather="weather" />;
    return (
      <div>
        <InputField handleChange={this.handleChange} />
        <Button variant="contained" onClick={this.getCityName}>
          Get weather in given city
        </Button>

        <Button
          variant="contained"
          onClick={e => this.getAllCitites(citiesArr)}
        >
          Weather in all cities
        </Button>

        {data.length > 0 ? (
          <Button onClick={() => this.sortByTemp()} variant="contained">
            Sort by temp
          </Button>
        ) : (
          ""
        )}
        <div className={classes.weatherList}>
          {data.name ? (
            <WeatherBox weather={data} key={data.id} />
          ) : (
            data.map(weather => (
              <WeatherBox weather={weather} key={weather.id} />
            ))
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(App);
